# Запуск
## Скоприровать репозиторий
```bash
git clone https://gitlab.com/bulatovbogdann/mlflow_homework.git
`````
## Зайти в папку с проектом 
```bash
cd mlflow_homework
`````
## Установить зависимости 
```bash
poetry install
`````
## Положить данные
В папку src/data/ положить данные Amazone Reviwe (назвать data.csv)
## Запустить сервер MLflow
```bash
poetry run mlflow server
`````
## Запустить эксперименты
```bash
poetry run python run_experiment.py --multirun preprocessing=tfidf,count,bert train=svm,logreg,random_forest
`````