# Введение
В данном отчёте был проведен ряд экспериментов для выявления наилучшей модели для классификации отзывов [Amazon](https://www.kaggle.com/datasets/kritanjalijain/amazon-reviews)

Список моделей:
- Support Vector Machine
- Logistic Regression
- Random Forest 

Методы векторизации:
- Term Frequency-Inverse Document Frequency
- Count Vectorization

Было запущено шесть экспериментов (каждая модель была оценена относительно каждого метода векторизации исходного текста). Результаты представленны на графиках ниже:
# Macro avg
![Пример изображения](assets/Macro_avg.png)
# Weighted avg
![Пример изображения](assets/Weighted_avg.png)
# Negative class
![Пример изображения](assets/Negative.png)
# Positive class
![Пример изображения](assets/Positive.png)

# Вывод:
Для задачи классификации отзывов Amazon наиболее оптимальным оказалось векторизовать данные с помощью метода **tfidf** с последующим применением модели **логистической регрессии**.