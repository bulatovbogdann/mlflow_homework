from pathlib import Path
import sys
import os 
import warnings
import logging 
import warnings  # noqa: F811

import hydra # type: ignore
from hydra.utils import instantiate  # type: ignore
import mlflow # type: ignore
from omegaconf import DictConfig, OmegaConf # type: ignore
import polars as pl  # type: ignore
from sklearn.model_selection import train_test_split 
import torch  # type: ignore
from torch.utils.data import DataLoader  # type: ignore

sys.path.append(os.path.abspath('src'))
from preprocessing import (preprocess_pipe,  # type: ignore
                           process_data_and_map_polarity)
from metrics import classification_report, conf_matrix # type: ignore
from models import batch_inference # type: ignore


warnings.filterwarnings("ignore")

loger = logging.getLogger(__name__)

@hydra.main(version_base=None, config_path="conf", config_name="config")
def run_experiment(cfg : DictConfig) -> None:
    
    # preprocessing
    loger.info('Preprocessing...')
    data_path = Path('src/data')
    df_raw = pl.read_csv(data_path / 'data.csv', n_rows=cfg.n_rows)
    df = process_data_and_map_polarity(df_raw)
    df_preprocessed= df.with_columns(
        pl.col('Review').map_elements(preprocess_pipe).alias('corpus')
        )
    
    train, test = train_test_split(
        df_preprocessed,
        test_size=0.3,
        shuffle=True,
        random_state=cfg.random_state
        )
    
    # vectorize
    loger.info(f'Vectorizing {cfg.preprocessing.type}')
    if cfg.preprocessing.type in ['tfidf', 'count', 'random_forest']:
        encoder = instantiate(cfg.preprocessing.encoder)
        train_embedings = encoder.fit_transform(train['corpus'].to_pandas().astype(str))
        test_embedings = encoder.transform(test['corpus'].to_pandas().astype(str)) 
        
    elif cfg.preprocessing.type == 'bert':
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')  # noqa: F841
        tokenizer = instantiate(cfg.preprocessing.tokenizer)
        bert_model = instantiate(cfg.preprocessing.model)
        train_dataloader = DataLoader(train['Review'].to_list(), batch_size=cfg.preprocessing.fixed_batch_size, shuffle=False)
        test_dataloader = DataLoader(test['Review'].to_list(), batch_size=cfg.preprocessing.fixed_batch_size, shuffle=False) 
        train_embedings = torch.concat([batch_inference(batch_data, tokenizer, bert_model) for batch_data in train_dataloader])
        test_embedings = torch.concat([batch_inference(batch_data, tokenizer, bert_model) for batch_data in test_dataloader])
    else:
        raise Exception('There is no such vectorizer')
    
    # experiments 
    loger.info(f'Run model {cfg.train.type}')
    mlflow.set_tracking_uri('http://127.0.0.1:5000')
    mlflow.set_experiment('Testing models') 
    
    run_name = f'{cfg.train.type}_{cfg.preprocessing.type}_run'
    with mlflow.start_run(run_name=run_name) as run:
        model = instantiate(cfg.train.model)
        model.fit(train_embedings, train['Polarity'])
        predict = model.predict(test_embedings)
        report = classification_report(test['Polarity'], predict, output_dict=True)
        
        mlflow.log_metric('accuracy', report.pop('accuracy'))
        for class_or_avg, metrics_dict in report.items():
            for metric, value in metrics_dict.items():
                mlflow.log_metric(class_or_avg + '_'+ metric, value)
                
        mlflow.log_params(OmegaConf.to_container(cfg, resolve=True)) 
        mlflow.sklearn.log_model(
            sk_model=model,
            input_example=test_embedings[:10],
            artifact_path=f'mlflow/{run_name}/model'
        )
        
        fig = conf_matrix(test['Polarity'], predict)
        mlflow.log_figure(fig, f'{run_name}_confusion_matrix.png') 
    
    
if __name__ == "__main__":
    run_experiment()