import numpy as np 
from matplotlib import pyplot as plt
from matplotlib.figure import Figure 
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.metrics import classification_report

def conf_matrix(y_true: np.ndarray, pred: np.ndarray) -> Figure:
    plt.ioff()
    fig, ax = plt.subplots(figsize=(5,5))
    ConfusionMatrixDisplay.from_predictions(y_true, pred, ax=ax, colorbar=False)
    ax.xaxis.set_tick_params(rotation=90)
    _ = ax.set_title('Confusion Matrix')
    plt.tight_layout()
    return fig
