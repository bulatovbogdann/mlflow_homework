import torch  # type: ignore
from transformers import AutoTokenizer, AutoModel # type: ignore

def batch_inference(batch: dict, tokenizer: AutoTokenizer, model: AutoModel):
    tokenized_batch = tokenizer(
        batch, padding=True,
        truncation=True,
        return_tensors='pt'
    )
    with torch.no_grad():
        hidden_batch = model(**tokenized_batch)
        batch_embeddings = hidden_batch.last_hidden_state[:, 0, :].detach().to('cpu')
        return batch_embeddings 