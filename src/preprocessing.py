import re 
from pathlib import Path 

import polars as pl
import spacy
from spacy.cli import download

try:
    nlp = spacy.load('en_core_web_sm', disable = ['ner', 'tagger', 'parser'])
except OSError:
    download("en_core_web_sm")
    nlp = spacy.load('en_core_web_sm', disable = ['ner', 'tagger', 'parser'])


def process_data_and_map_polarity(df):
    df.columns = ['Polarity', 'Title', 'Review']
    
    return df.select('Polarity', 'Review').with_columns(
    pl.col('Polarity').map_elements(
        lambda polarity: 'Negative' if polarity==1 else 'Positive'
    )
)

def lemmatize_pipe(doc):
    lemma_list = [str(tok.lemma_).lower() for tok in doc if tok.is_alpha and ~tok.like_url] 
    return lemma_list

def preprocess_pipe(texts):
    preproc_pipe = []
    for doc in nlp.pipe([texts], batch_size=20):
        preproc_pipe.extend(lemmatize_pipe(doc))
    return preproc_pipe
